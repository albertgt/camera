# Camera

![d750](https://cdn-4.nikon-cdn.com/e/Q5NM96RZZo-YRYNeYvAi9beHK4x3L-8iSKFuXbTDiVzKiZ_IJQu2eA==/Views/1543_D750_side_left.png)<br>
## Camera Body [Nikon D750 Full Frame](https://www.nikonusa.com/en/nikon-products/product/dslr-cameras/1543/d750.html)<br>
> [Retail $1699](https://www.bhphotovideo.com/c/product/1082599-REG/nikon_d750_dslr_camera_body.html)<br>
> [Ebay Used < $1000](https://www.ebay.com/sch/i.html?_from=R40&_nkw=nikon+d750&_sacat=0&LH_ItemCondition=3000&rt=nc&LH_Sold=1&LH_Complete=1)<br>
<br>

## 3 Primary Lenses

![50](https://cdn-4.nikon-cdn.com/e/Q5NM96RZZo-fTYlSZPBjlMhlFa1VHARsAMnUXr0i75XkUxlZjQlHcBTouveKdWu7-K5JEdqBQrw=/Views/1902_AF_NIKKOR_50mm_f-1_4D.png)<br>
### Portraits, people [Nikkor 50mm 1.4 AF-D](https://kenrockwell.com/nikon/5014af.htm)<br>
> [Retail $350](https://www.bhphotovideo.com/c/product/97413-USA/Nikon_1902_AF_Nikkor_50mm_f_1_4D.html)<br>
> [Ebay Used ~$150](https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2334524.m570.l1313.TR2.TRC1.A0.H0.Xnikkor+50mm+1.4+af-d.TRS0&_nkw=nikkor+50mm+1.4+af-d&_sacat=0&LH_TitleDesc=0&_osacat=0&_odkw=nikon+d750&LH_Complete=1&LH_ItemCondition=3000&rt=nc&LH_Sold=1)<br>

---

![24](https://cdn-4.nikon-cdn.com/e/Q5NM96RZZo-fTYlSZPBjlMhlFa1VHARsAMnUXr0j5JVxgR3e_5IxIXDPsWd2tAV5WNpiqfkbBhs=/Views/1919_AF-NIKKOR-24mm-f-2.8D_front.png)<br>
### Landscape [Nikkor 24mm 2.8 AF-D](https://kenrockwell.com/nikon/24mm-f28-afd.htm)<br>
> [Retail $390](https://www.bhphotovideo.com/c/product/66980-USA/Nikon_1919_Wide_Angle_AF_Nikkor.html)<br>
> [Ebay Used ~$160](https://www.ebay.com/sch/i.html?_from=R40&_trksid=m570.l1313&_nkw=nikkor+24mm+2.8+af-d&_sacat=0&LH_TitleDesc=0&_osacat=0&_odkw=nikkor+50mm+1.4+af-d&LH_Complete=1&LH_Sold=1&LH_TitleDesc=0)<br>

---

![80-200](https://cdn-4.nikon-cdn.com/e/Q5NM96RZZo-fTYlSZPBjlMhlFa1VHARsAMnUXr0q65WMPsDRrlbFNf1F3jxZxDRvh-7on3nbKke94FDEoTIteNmVhDRvMMpC/Views/1986_AF-Zoom-NIKKOR-80-200mm-f-2.8-ED_front.png)<br>
### Weddings, Sports, Events [Nikkor 80-200 2.8 AF-D](https://kenrockwell.com/nikon/80200.htm)<br>
> [Retail $1200](https://www.bhphotovideo.com/c/product/124669-USA/Nikon_1986_AF_Zoom_Nikkor_80_200mm_f_2_8D.html)<br>
> [Ebay Used ~$400](https://www.ebay.com/sch/i.html?_from=R40&_trksid=m570.l1313&_nkw=nikkor+80-200+2.8+af-d&_sacat=0&LH_TitleDesc=0&_osacat=0&_odkw=nikkor+24mm+2.8+af-d&LH_Complete=1&LH_Sold=1&LH_TitleDesc=0)<br>

---

![Flash](https://cdn-4.nikon-cdn.com/e/Q5NM96RZZo-aQIVEfvQ997L3sEHoBd-VCJRKp0azfIGIA2qjHVulZXiEO1kIf-SI/Views/4808_SB-700-AF-Speedlight-front.png)<br>
### Flash/Strobe
> [Nikon SB-700 Speedlite - Retail $350](https://www.bhphotovideo.com/c/product/734997-USA/Nikon_4808_SB_700_Speedlight_Shoe_Mount.html)<br>
> [Neewer 750II Speedlite - Retail $50 ](https://www.amazon.com/Neewer-750II-Speedlite-Display-Cameras/dp/B00GE4MNQA/ref=sr_1_sc_2?ie=UTF8&qid=1534946051&sr=8-2-spell&keywords=neeweer+flash)<br>

---

### Why AF-D?<br>
The AF-D era lenses are a secretly affordable option to get professional build quality and performance. Each AF-D lens is hefty and built like a tank. The quick autofocus speed is driven by the camera body so you don't have to worry about any motors breaking inside the lens. The AF-D era of the 1990s glass is still high quality professional glass that is produced as new lenses even today. The main difference from AF-S, AF-P is these newer designations just add in motors in the lens itself and other features like vibration reduction and remove the ability to set aperture manually via an aperture ring on the lens. AF-D lets you have the high quality glass without any restrictions or bloat features.


### Insightful Readings<br>
https://kenrockwell.com/tech/basics.htm<br>
http://www.mountainlight.com/articles.html<br>
https://www.amazon.com/Examples-Making-Photographs-Ansel-Adams/dp/082121750X<br>
